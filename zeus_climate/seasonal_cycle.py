"""
Seasonal cycle analysis tools
"""


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest
import datetime

import numpy
import pandas


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

_mnjday = 1
_mxjday = 365
_mxjday2 = 366
_midjday = _mxjday2 / 2

_fakeyear = 2104
_season_name = "season"

##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def schematic_rainyseries(nday=_mxjday, dpic=180, mpic=30):
    """
    >>> sr = schematic_rainyseries()
    >>> sr.loc[178:182]
    178    29.6
    179    29.9
    180    30.0
    181    29.9
    182    29.6
    dtype: float64

    >>> sr.loc[160:165]
    160    0.0
    161    0.0
    162    0.0
    163    1.1
    164    4.4
    165    7.5
    dtype: float64
    >>> sr.loc[195:200]
    195    7.5
    196    4.4
    197    1.1
    198    0.0
    199    0.0
    200    0.0
    dtype: float64
    """
    jdays = numpy.arange(nday) + _mnjday
    x = jdays - dpic
    y = mpic - 0.1*x**2
    y[y <= 0] = 0
    res = pandas.Series(y, index=jdays)
    return res
    

def is_jday(jd):
    """
    >>> is_jday(0)
    False
    >>> is_jday(180)
    True
    >>> is_jday(180.)
    False
    >>> is_jday(380.)
    False
    """
    cond0 = isinstance(jd, (int, numpy.int64))
    cond1 = _mnjday <= jd <= _mxjday2
    res = cond0 and cond1
    return res
    

def doy2fakedatetimes(doys):
    """
    >>> ds = doy2fakedatetimes(numpy.arange(1,366))
    >>> ds2 = doy2fakedatetimes(numpy.arange(1,367))
    >>> print(ds[0])
    2105-01-01 00:00:00
    >>> print(ds[-1])
    2105-12-31 00:00:00
    >>> print(ds2[0])
    2104-01-01 00:00:00
    >>> print(ds2[-1])
    2104-12-31 00:00:00
    >>> len(set(numpy.diff(ds)))
    1
    >>> len(set(numpy.diff(ds2)))
    1
    """
    if doys.size == _mxjday2:
        fkyr = _fakeyear
    elif doys.size == _mxjday:
        fkyr = _fakeyear + 1
    else:
        raise ValueError("Exp 365 or 366 day of year. Got {0}".format(doys.size))
    assert (numpy.diff(doys) == 1).all(), "All day of year are expected"
        
    res = pandas.date_range('01-01-{0}'.format(fkyr), '31-12-{0}'.format(fkyr))
    assert res.size == doys.size
    
    return res
    
def decadal(x):
    """
    >>> dates = pandas.date_range('2010-01-01', '2010-12-31')
    >>> decades = sorted(set(dates.map(decadal).values))
    >>> decades = pandas.Series(decades, index=decades)
    >>> for i in [0, 17, -1]: print(decades.index[i])
    2010-01-05 00:00:00
    2010-06-25 00:00:00
    2010-12-25 00:00:00
    >>> len(decades)
    36
    
    """
    dy = x.day
    if dy <= 11:
        ndy = 5
    elif dy <= 21:
        ndy = 15
    else:
        ndy = 25
    
    res = datetime.datetime(x.year, x.month, ndy, x.hour)
    
    return res
    
def name2algo(name):
    """
    >>> [name2algo(i) for i in SEASON_ALGORYTHMS]
    ['agrhymet_season', 'agro_season', 'hydro_season', 'liebmann_season', 'wanglinho_season']
    """
    if _season_name in name:
        res = name
    else:
        res = "_".join([name, _season_name])
        
    return res
    
def algo2name(name):
    """
    >>> algo2name("nimp")
    Traceback (most recent call last):
        ...
    AssertionError: algorythm must contain season:
    nimp
    >>> algo2name("a_season")
    'a'
    >>> SEASON_ALGORYTHMS
    ['agrhymet', 'agro', 'hydro', 'liebmann', 'wanglinho']
    """
    assert _season_name in name, "algorythm must contain {0}:\n{1}".format(_season_name, name)
    res = name.replace(_season_name, "").strip("_")
    return res
    
    
def continued_index_for_seasonal_rolling(idx):
    """
    Function doc
    >>> 'afarie'
    """
    dts = numpy.diff(idx)
    assert len(set(dts)) == 1
    dt = dts[0]
    minidx = idx.min()
    maxidx = idx.max()
    toadd = (numpy.arange(idx.size)+1) * dt
    befidx = numpy.sort(minidx - toadd)
    aftidx = numpy.sort(maxidx + toadd)
    res = numpy.concatenate([befidx, idx, aftidx])
    assert len(set(numpy.diff(res))) == 1
    return res
    
def continued_series_for_seasonal_rolling(obj):
    """
    Function doc
    >>> 'afarie'
    """
    newidx = continued_index_for_seasonal_rolling(obj.index)
    newobj = pandas.concat([obj]*3)
    # ~ print(newidx)
    # ~ print(newobj)
    if isinstance(obj, pandas.DataFrame):
        res = pandas.DataFrame(newobj.values, index = newidx, columns=obj.columns)
        # ~ print(res.loc[obj.index].index == obj.index)
        assert (res.reindex(obj.index) == obj).all().all()
    elif isinstance(obj, pandas.Series):
        res = pandas.Series(newobj.values, index = newidx, name=obj.name)
        assert (res.reindex(obj.index) == obj).all()
    else:
        raise ValueError("Dont know what to do with {0}\n{1}".format(obj, type(obj)))
    return res


def doyoffset(doy, offset=_midjday):
    """
    Function doc
    >>> 'afarie'
    """
    if doy < offset:
        res = doy
    else:
        res = doy - _mxjday2
    return res
    

    
##======================================================================================================================##
##                RAINY SEASON CLASS                                                                                    ##
##======================================================================================================================##

class RainySeason:
    """ Class doc
    >>> r=RainySeason(180,250)
    >>> print(r)
    (180, 250)
    >>> r.length()
    70
    >>> r1 = RainySeason(180,250)
    >>> r==r1
    True
    >>> r1 = RainySeason(180,251)
    >>> r==r1
    False
    >>> r2 = RainySeason(180,251)
    >>> r==r2
    False
    >>> r1==r2
    True
    
    >>> rnan = RainySeason(numpy.nan,numpy.nan)
    >>> rnan1 = RainySeason(numpy.nan,numpy.nan)
    >>> r==rnan
    False
    >>> rnan1==rnan
    True
    """
    
    def __init__(self, start, end):
        """ Class initialiser """
        self.end = end
        self.start = start
        if not self.isnull():
            assert is_jday(self.start), "start={0} is not valid julian day".format(self.start)
            assert is_jday(self.end), "end={0} is not valid julian day".format(self.end)
            assert self.end > self.start, "start={0} end={1} not possible".format(self.start, self.end)
        
    def __eq__(self, other):
        """See main"""
        assert isinstance(other, RainySeason)
        if self.isnull():
            res = other.isnull()
        else:        
            cond1 = self.start == other.start 
            cond2 = self.end == other.end
            res = cond1 and cond2
        return res
        
    def __str__(self):
        """Representation for print"""
        res = str(tuple([self.start, self.end]))
        return res
        
    def isnull(self):
        """return true if nan"""
        res = numpy.isnan(self.start) and numpy.isnan(self.end)
        return res
        
    def length(self):
        """Length of the rainy season"""
        res = self.end - self.start
        return res

##======================================================================================================================##
##                ALGORYTHMS                                                                                            ##
##======================================================================================================================##

class YearDailySeries(pandas.Series):
    """ Class doc
    >>> ds = pandas.date_range("2000-01-01", "2000-12-31")
    >>> vs = numpy.zeros(ds.size)
    >>> vs = [numpy.random.exponential(scale=15) if (100 < j < 190) else 0 for i, j in zip(vs, ds.dayofyear)]
    >>> ts = pandas.Series(vs, index=ds)
    >>> ts = YearDailySeries(ts)
    >>> rs = ts.liebmann_season()
    >>> 100 <= rs.start <=110
    True
    >>> 180 <= rs.end <=190
    True
    
    >>> rs = ts.hydro_season()
    >>> 100 <= rs.start <=110
    True
    >>> 180 <= rs.end <=190
    True
    
    >>> rs = ts.agro_season()
    >>> rs.start >=121
    True
    >>> rs.end
    244
    
    >>> rs = ts.agrhymet_season()
    >>> 95 <= rs.start <=106
    True
    >>> rs.end ==249
    True
    
    >>> rs = ts.wanglinho_season()
    >>> 90 <= rs.start <=110
    True
    >>> 180 <= rs.end <=200
    True
    
    >>> vs = [numpy.random.exponential(scale=15) if (140 < j < 260) else 0 for i, j in zip(vs, ds.dayofyear)]
    >>> ts = pandas.Series(vs, index=ds)
    >>> ts = YearDailySeries(ts)
    >>> rs = ts.liebmann_season()
    >>> 135 <= rs.start <=150
    True
    >>> 255 <= rs.end <=265
    True
    
    >>> rs = ts.hydro_season()
    >>> 135 <= rs.start <=150
    True
    >>> 255 <= rs.end <=265
    True
    
    >>> rs = ts.agro_season()
    >>> 135 <= rs.start <=145
    True
    >>> 275 <= rs.end <=285
    True
    
    >>> rs = ts.agrhymet_season()
    >>> 135 <= rs.start <=150
    True
    >>> 250 <= rs.end <=290
    True
    
    >>> rs = ts.wanglinho_season()
    >>> 130 <= rs.start <=150
    True
    >>> 250 <= rs.end <=270
    True
    
   
    
    >>> sr = schematic_rainyseries()
    >>> sr = YearDailySeries(sr)
    >>> print(sr.liebmann_season())
    (163, 196)
    >>> print(sr.hydro_season())
    (164, 196)
    >>> print(sr.hydro_season(10))
    (166, 194)
    >>> print(sr.hydro_season(50))
    (nan, nan)
    
    >>> print(sr.agro_season())
    (164, 244)
    
    >>> print(sr.agrhymet_season())
    (166, 248)
    
    >>> print(sr.wanglinho_season())
    (161, 196)
    
    >>> sr2 = pandas.Series(schematic_rainyseries(ds.size).values,index=ds)
    >>> print(YearDailySeries(sr2).liebmann_season())
    (163, 196)
    >>> print(YearDailySeries(sr2).hydro_season())
    (164, 196)
    
    >>> sr3 = pandas.Series(0,index=ds)
    >>> print(YearDailySeries(sr3).liebmann_season())
    (nan, nan)
    >>> print(YearDailySeries(sr3).hydro_season())
    (nan, nan)
    
    
    >>> sr4 = pandas.Series(0,index=ds)
    >>> print(YearDailySeries(sr4).liebmann_season())
    (nan, nan)
    >>> print(YearDailySeries(sr4).hydro_season())
    (nan, nan)
    
    >>> ix = numpy.arange(5,350)
    >>> sr = schematic_rainyseries()
    >>> sr4 = pandas.Series(sr.loc[ix],index=sr.index)
    >>> print(YearDailySeries(sr4).liebmann_season())
    (163, 196)
    
    >>> ix = numpy.arange(5,350)
    >>> sr = schematic_rainyseries()
    >>> sr = pandas.Series(sr.values, doy2fakedatetimes(sr.index))
    >>> sr4 = YearDailySeries(sr.iloc[ix])
    
    >>> print(sr4.liebmann_season())
    (163, 196)
    
    >>> sr = schematic_rainyseries()
    >>> yds = YearDailySeries(sr)
    >>> pandas.Series({i: yds.apply_algorythm(i) for i in SEASON_ALGORYTHMS}).sort_index()
    agrhymet     (166, 248)
    agro         (164, 244)
    hydro        (164, 196)
    liebmann     (163, 196)
    wanglinho    (161, 196)
    dtype: object


    """
    
    def __init__(self, *args, **kwargs):
        """ Class initialiser """
        obj = pandas.Series(*args, **kwargs)
        
        if not obj.size in (_mxjday, _mxjday2):#
            yr = list(set(obj.index.year))
            hr = list(set(obj.index.hour))
            assert len(yr) == 1 and len(hr) == 1, "if year is not complte, please gie only one year got:yr={0} hr={1}".format(yr, hr)
            yr = yr[0]
            hr = hr[0]
            st = "{00}/01/01 {01}:00:00".format(yr, hr)
            ed = "{00}/12/31 {01}:00:00".format(yr, hr)
            dates = pandas.date_range(st, ed)
            obj = pandas.Series(obj, index=dates)
            assert obj.size in (_mxjday, _mxjday2), "exp 365 or 366, got {0}\n{1}".format(obj.size, obj.head())
            
            
        if not hasattr(obj.index, "dayofyear"):
            vals = obj.values
            dates = doy2fakedatetimes(obj.index)
            obj = pandas.Series(vals, index=dates)
        
        pandas.Series.__init__(self, obj)

    def get_algorythm(self, name):
        """Get the algorythm method"""
        algo = name2algo(name)
        res = getattr(self, algo)
        # ~ res = {k.replace(_season_name, "").strip("_") : v for k, v in res.items()}
        return res
    
    def apply_algorythm(self, name, **kwargs):
        """Apply the algorythm"""
        fun = self.get_algorythm(name=name)
        res = fun(**kwargs)
        # ~ res = pandas.Series(res, index=sorted(funs))
        return res
        

    def liebmann_season(self, return_ts=False):
        """Define in Liebmann et al. papers
        Liebmann et al. "Seasonality of African Precipitation from 1996 to 2009". Journal of Climate, 2012
        """
        idx = self.index.dayofyear
        
        if self.isnull().all() or self.sum() == 0:
            #~ print("Warning index pb: {0} {1} : nan value".format(idx.min(), idx.max()))
            res_series = pandas.Series(None, index=idx)
        else:
            data = self.fillna(0)
            assert data.min() >= 0, "Rains must be positive"
            mrr = data.mean()
            diff = data - mrr
            cumdif = diff.cumsum()
            res_series = pandas.Series(cumdif.values, index=idx)
        
        if return_ts:
            res = res_series
        else:
            st = res_series.idxmin()
            ed = res_series.idxmax()
            if ed <= st or  numpy.isnan(ed) or numpy.isnan(st):
                st = numpy.nan
                ed = numpy.nan
            res = RainySeason(start=st, end=ed)

        return res
        
    def hydro_season(self, threshold=2.5):
        """Hydrological definition, simply apply a threshold"""
        cond = self > threshold
        valid = self[cond]
        validx = valid.index.dayofyear
        
        st = validx.min()
        ed = validx.max()
        if ed <= st or  numpy.isnan(ed) or numpy.isnan(st):
            st = numpy.nan
            ed = numpy.nan
            
        res = RainySeason(start=st, end=ed)

        return res
        

    def agro_season(self, threshold=20, nb_of_days=3):
        """Agronomic season definition"""
        doy = self.index.dayofyear

        ts_7days = self.rolling(window=7).sum().fillna(0).sort_index(ascending=False) # seven days rainfall totals
        dry_seq = (ts_7days.rolling(window=30).min().fillna(0) <= 1e-8).sort_index() # if during the last 30 days, no 7 days totals, then it is a dry spell

        r_ts = self.sort_index(ascending=False) # reversed series
        cum = r_ts.rolling(window=nb_of_days).sum().sort_index() #rolling sum

        condstart = (cum >= threshold) & (doy >= 121) & (doy < 244) & (dry_seq == False) # start when the rolling total is higher to threshold, and when no in a dry sequence

        cum20 = self.rolling(window=20).sum() #twenty day totals
        condend = (cum20 <= 1e-8) & (doy >= 244) # end when the rolling sum during 20 day is equal to zero. Last after 20 day of zero rainfall
        
        st = doy[condstart].min()
        ed = doy[condend].min()
        
        if ed <= st or  numpy.isnan(ed) or numpy.isnan(st):
            st = numpy.nan
            ed = numpy.nan
        
        res = RainySeason(start=st, end=ed)

        return res


    def agrhymet_season(self):
        """Agrhymet season definition"""
        ts_10days = self.groupby(lambda x: decadal(x)).sum()
        # ~ times_10days = pandas.Series(ts_10days.index, index=ts_10days.index)
        doy_10days = ts_10days.index.dayofyear

        r1 = ts_10days.shift(-1)
        r2 = ts_10days.shift(-2)

        condstart = (ts_10days >= 25) & (r1 >= 20) & (r2 >= 20)
        condend = (ts_10days < 25) & (r1 < 20) & (r2 < 20) & (doy_10days >= 244)
        # ~ assert 0, "use where or not" --> not usse
        
        st = doy_10days[condstart].min()
        ed = doy_10days[condend].min()
        
        if ed <= st or  numpy.isnan(ed) or numpy.isnan(st):
            st = numpy.nan
            ed = numpy.nan
        
        res = RainySeason(start=st, end=ed)
        return res


    def wanglinho_season(self, nb_harmonic=12):
        """Wang et al. Season definition"""
        pentad = self.resample('5D', closed="left", label="left", loffset="2D")
        exp = self.sum() #min_count=1
        got = pentad.sum().sum()
        absreldif = abs(exp - got) / exp
        assert absreldif <= 0.2 or numpy.isnan(exp) or exp == 0., "abs dif may be to high: {0:.1%}.{1} vs {2}".format(absreldif, exp, got)
            
        pentad = pentad.mean()
        
        if pentad.isnull().all():
            filtered = pentad
        else:
            if pentad.size % 2 == 1:
                pentad = pentad.iloc[:-1]
            
            w = numpy.fft.rfft(pentad.fillna(0))
            w2 = w.copy()
            w2[nb_harmonic+1:] = 0
            filtered = numpy.fft.irfft(w2)
        
        filtered = pandas.Series(filtered, index=pentad.index)
        
        cond = filtered >= 2.4
        valid = filtered[cond]
        validx = valid.index.dayofyear
        
        st = validx.min()
        ed = validx.max()
        if ed <= st or  numpy.isnan(ed) or numpy.isnan(st):
            st = numpy.nan
            ed = numpy.nan
        
        res = RainySeason(start=st, end=ed)
        
        return res
        

##======================================================================================================================##
##                OTHER CONSTANTS                                                                                       ##
##======================================================================================================================##

SEASON_ALGORYTHMS = [algo2name(i) for i in dir(YearDailySeries) if _season_name in i]

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)

    #+++++++++++++++++++++++++++++++#
    #    EXAMPLE                    #
    #+++++++++++++++++++++++++++++++#
    
    else:
        ds = pandas.date_range("2000-01-01", "2000-12-31")
        vs = numpy.zeros(ds.size)
        vs = [numpy.random.exponential(scale=15) if (100 < j < 190) else 0 for i, j in zip(vs, ds.dayofyear)]
        ts = pandas.Series(vs, index=ds)
        rs = YearDailySeries(ts)
        
        for nm in SEASON_ALGORYTHMS:
            season = rs.apply_algorithm(name=nm)
            print(nm, season)
        
