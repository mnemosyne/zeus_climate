"""
For create and ploting standardized indices
"""
__license__ = "GNU GPL"



##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import doctest


from zeus_climate.periods import * # Period, Periods, auto_periods_index, period_slice
#print(1)
from zeus_climate.boxes import * # Box, is_box, LATKEY, LONKEY
#print(2)
from zeus_climate.seasonal_cycle import * # RainySeason, SEASON_ALGORYTHMS, YearDailySeries, continued_series_for_seasonal_rolling, doyoffset
#print(3)

from zeus_climate.season import * # MONTHS_FIRST, find_season

#print(4)
from zeus_climate.series import * # cooling_degree_day, heating_degree_day, StdClimateIndex
#print(5)


##======================================================================================================================##
##                HYDRO-CLIAMTIC UINTENSITY                                                                             ##
##======================================================================================================================##






##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--example', default=False, action='store_true')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTEST                    #
    #+++++++++++++++++++++++++++++++#

    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
    
    #+++++++++++++++++++++++++++++++#
    #    EXAMPLES                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.example:
        box = Box("sahel")
        per = Period("rg")
        pers = Periods("wa")
        
        for bn in ("wa", "sahel", "east sahel"):
            print(bn, is_box(bn))
    
        print(LATKEY, LONKEY)
