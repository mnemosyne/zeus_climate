"""
Tools for using month analysis, season diurnal cycle
"""

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import locale
import calendar
import doctest
# ~ from numbers import Number



# ~ from collections import Iterable
import datetime
import argparse


import numpy
import pandas


##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

# ~ LANGS = ("fr", "en", "hg")
LANGS = ("fr", "en")


TD_1HR = datetime.timedelta(hours=1)
TD_1DY = datetime.timedelta(days=1)
H_IN_1D = int(TD_1DY / TD_1HR)


D_IN_1YR = 365.25

# ~ assert (S_IN_1H / S_IN_1M) == 60, "pb code"
# ~ assert (S_IN_1D / S_IN_1H) == H_IN_1D, "pb code"

N_MONTH = 12
MONTHS_RANGE = numpy.arange(N_MONTH) + 1

# ~ MONTH_NAMES = ("January", "February", "March", "April", "May", "June", \
               # ~ "July", "August", "September", "October", "November", "December")

# ~ FR_MON_NMS = ("janvier", 'fevrier', 'mars', 'avril', 'mai', 'juin', \
              # ~ 'juillet', 'aout', 'septembre', 'octobre', 'novembre', 'decembre')

_am = "am"
_pm = "pm"
_ev = "ev"
_ng = "ng"
    
##======================================================================================================================##
##                FUNCTIONS  LOACALE                                                                                    ##
##======================================================================================================================##

    
def init_locale():
    """Init the locale and calendar. Seem necessary to avoid a first error when invoking a new locale."""
    _deflocale = locale.getdefaultlocale()[0]
    # ~ _deflocale = "nimp"
    i = 0
    while i < 10:
        i += 1
        printerr = bool(i > 1)
        res = try_locale(lnm=_deflocale, printerror=printerr)
        if res:
            break #res == True mean that the locale is succesfully loaded
    
    else: #if the break is not executed
        raise ValueError("i={0}\ninit not succesfully done".format(i))
        
    return res
            
    
def try_locale(lnm, printerror=False):
    """Test different locale setting"""
    try:
        with calendar.different_locale(lnm):
            pass
        res = True
    except locale.Error as err:
        res = False
        if printerror:
            print(err)
    
    return res
        
def valid_locales():
    """Return the locale that could be succesfully loaded
    >>> d = valid_locales()
    >>> sorted(d) == sorted(LANGS)
    True
    """
    locs = pandas.Series(locale.locale_alias)
    msk = locs.apply(try_locale)
    locs = locs[msk]
    
    res = {}
    for lg in LANGS:
        if lg in locale.getdefaultlocale()[0]:
            val = locale.getdefaultlocale()[0]
        else:
            lidx = [i for i in locs.index if i[:2] == lg]
            llc = locs[lidx]
            if len(set(llc.values)) == 1:
                val = llc.iloc[0]
            elif lg in llc:
                val = llc.loc[lg]
            else:
                print("{0} not found".format(lg))
                val = None
        res[lg] = val
    
    # ~ print(res)
    res = {k:v for k, v in res.items() for k, v in res.items() if v is not None}
    
    return res


##======================================================================================================================##
##                MAKE MONTHS                                                                                           ##
##======================================================================================================================##

def _month_series(lang='en'):
    """Make months Series
    >>> MONTHS
    1       January
    2      February
    3         March
    4         April
    5           May
    6          June
    7          July
    8        August
    9     September
    10      October
    11     November
    12     December
    dtype: object
    >>> MONTHS_SHORT
    1     Jan
    2     Feb
    3     Mar
    4     Apr
    5     May
    6     Jun
    7     Jul
    8     Aug
    9     Sep
    10    Oct
    11    Nov
    12    Dec
    dtype: object
    >>> MONTHS_FIRST
    1     J
    2     F
    3     M
    4     A
    5     M
    6     J
    7     J
    8     A
    9     S
    10    O
    11    N
    12    D
    dtype: object
    >>> months_df = _month_series(lang = 'fr')
    >>> months_df.loc[1]
    'janvier'
    """
    if lang in LANGS: #, "Exp one of {0}".format(LANGS)
        llocale = valid_locales()[lang]
        with calendar.different_locale(llocale):
            months = {i: calendar.month_name[i] for i in MONTHS_RANGE}

    else:
        months = {i: calendar.month_name[i] for i in MONTHS_RANGE}

    res = pandas.Series(months, index=sorted(months))

    assert res.size == N_MONTH and (res.index == MONTHS_RANGE).all()
    
    return res

try:
    MONTHS = _month_series(lang="en")
except:# Error as err:
    MONTHS = _month_series(lang=None)

MONTHS_SHORT = MONTHS.apply(lambda x: x[:3].title())
MONTHS_FIRST = MONTHS.apply(lambda x: x[0].upper())

##======================================================================================================================##
##                FUNCTIONS  SEASON                                                                                     ##
##======================================================================================================================##

def std_month_number(n):
    """
    >>> pandas.Series({i: std_month_number(i) for i in range(24)})
    0     12
    1      1
    2      2
    3      3
    4      4
    5      5
    6      6
    7      7
    8      8
    9      9
    10    10
    11    11
    12    12
    13     1
    14     2
    15     3
    16     4
    17     5
    18     6
    19     7
    20     8
    21     9
    22    10
    23    11
    dtype: int64
    """
    res = (n - 1)%N_MONTH + 1
    return res
    


def cut_seasons(month_start=3, length=3):
    """
    Return list of months, cutted by seasons

    >>> cut_seasons(1,1)
    Traceback (most recent call last):
            ...
    AssertionError: pb season length : 1
    >>> (cut_seasons(1,2) == numpy.array([[1, 2], [3, 4], [5, 6], [7, 8], [9, 10], [11, 12]])).all()
    True
    >>> (cut_seasons(1,3) == numpy.array([[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12]])).all()
    True
    >>> (cut_seasons(1,4)  == numpy.array([[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]])).all()
    True
    >>> cut_seasons(1,5)
    Traceback (most recent call last):
     ...
    AssertionError: pb season length : 5
    >>> (cut_seasons(1,6)  == numpy.array([[1, 2, 3, 4, 5, 6], [7, 8, 9, 10, 11, 12]])).all()
    True
    >>> (cut_seasons(3,3)  == numpy.array([[3, 4, 5], [6, 7, 8], [9, 10, 11], [12, 1, 2]])).all()
    True
    >>> (cut_seasons(4,6)  == numpy.array([[4, 5, 6, 7, 8, 9], [10, 11, 12, 1, 2, 3]])).all()
    True
    """
    assert 1 < length <= 6 and N_MONTH % length == 0, "pb season length : {0}".format(length)
    nseason = int(N_MONTH / length)
    x1 = range(month_start, month_start + N_MONTH)
    x2 = [std_month_number(i) for i in x1]
    x2 = numpy.array(x2)
    res = x2.reshape(nseason, length)
    assert (numpy.sort(res.flatten()) == MONTHS_RANGE).all()
    return res


def seasons_df(month_start=3, length=3):
    """
    Function doc
    >>> seasons_df(1,2)
    1     JF
    2     JF
    3     MA
    4     MA
    5     MJ
    6     MJ
    7     JA
    8     JA
    9     SO
    10    SO
    11    ND
    12    ND
    dtype: object

    >>> seasons_df(1,3)
    1     JFM
    2     JFM
    3     JFM
    4     AMJ
    5     AMJ
    6     AMJ
    7     JAS
    8     JAS
    9     JAS
    10    OND
    11    OND
    12    OND
    dtype: object
    >>> seasons_df(1,4)
    1     JFMA
    2     JFMA
    3     JFMA
    4     JFMA
    5     MJJA
    6     MJJA
    7     MJJA
    8     MJJA
    9     SOND
    10    SOND
    11    SOND
    12    SOND
    dtype: object

    >>> seasons_df(1,6)
    1     JFMAMJ
    2     JFMAMJ
    3     JFMAMJ
    4     JFMAMJ
    5     JFMAMJ
    6     JFMAMJ
    7     JASOND
    8     JASOND
    9     JASOND
    10    JASOND
    11    JASOND
    12    JASOND
    dtype: object
    >>> seasons_df(3,3)
    1     DJF
    2     DJF
    3     MAM
    4     MAM
    5     MAM
    6     JJA
    7     JJA
    8     JJA
    9     SON
    10    SON
    11    SON
    12    DJF
    dtype: object
    >>> seasons_df(4,6)
    1     ONDJFM
    2     ONDJFM
    3     ONDJFM
    4     AMJJAS
    5     AMJJAS
    6     AMJJAS
    7     AMJJAS
    8     AMJJAS
    9     AMJJAS
    10    ONDJFM
    11    ONDJFM
    12    ONDJFM
    dtype: object
    """
    cuts = cut_seasons(month_start=month_start, length=length)
    data = {"".join(MONTHS_FIRST.loc[isea]): isea for isea in cuts}
    res = pandas.Series({i: k for k, v in data.items() for i in v}, index=MONTHS_FIRST.index)

    return res


def possible_seasons(length):
    """
    >>> possible_seasons(2)
    JF      (1, 2)
    FM      (2, 3)
    MA      (3, 4)
    AM      (4, 5)
    MJ      (5, 6)
    JJ      (6, 7)
    JA      (7, 8)
    AS      (8, 9)
    SO     (9, 10)
    ON    (10, 11)
    ND    (11, 12)
    DJ     (12, 1)
    dtype: object
    >>> possible_seasons(3)
    JFM       (1, 2, 3)
    FMA       (2, 3, 4)
    MAM       (3, 4, 5)
    AMJ       (4, 5, 6)
    MJJ       (5, 6, 7)
    JJA       (6, 7, 8)
    JAS       (7, 8, 9)
    ASO      (8, 9, 10)
    SON     (9, 10, 11)
    OND    (10, 11, 12)
    NDJ     (11, 12, 1)
    DJF      (12, 1, 2)
    dtype: object
    >>> possible_seasons(4)
    JFMA       (1, 2, 3, 4)
    FMAM       (2, 3, 4, 5)
    MAMJ       (3, 4, 5, 6)
    AMJJ       (4, 5, 6, 7)
    MJJA       (5, 6, 7, 8)
    JJAS       (6, 7, 8, 9)
    JASO      (7, 8, 9, 10)
    ASON     (8, 9, 10, 11)
    SOND    (9, 10, 11, 12)
    ONDJ    (10, 11, 12, 1)
    NDJF     (11, 12, 1, 2)
    DJFM      (12, 1, 2, 3)
    dtype: object
    >>> possible_seasons(6)
    JFMAMJ       (1, 2, 3, 4, 5, 6)
    FMAMJJ       (2, 3, 4, 5, 6, 7)
    MAMJJA       (3, 4, 5, 6, 7, 8)
    AMJJAS       (4, 5, 6, 7, 8, 9)
    MJJASO      (5, 6, 7, 8, 9, 10)
    JJASON     (6, 7, 8, 9, 10, 11)
    JASOND    (7, 8, 9, 10, 11, 12)
    ASONDJ    (8, 9, 10, 11, 12, 1)
    SONDJF    (9, 10, 11, 12, 1, 2)
    ONDJFM    (10, 11, 12, 1, 2, 3)
    NDJFMA     (11, 12, 1, 2, 3, 4)
    DJFMAM      (12, 1, 2, 3, 4, 5)
    dtype: object
    """
    cuts = [numpy.arange(s, e) for s, e in zip(MONTHS_RANGE, MONTHS_RANGE + length)]
    cuts = [[std_month_number(i) for i in x1] for x1 in cuts]
    idx = ["".join(MONTHS_FIRST.loc[isea]) for isea in cuts]
    res = pandas.Series([tuple(i) for i in cuts], index=idx)
    assert res.size == N_MONTH and not res.index.duplicated().any() and not res.duplicated().any() #no duplicated in index
    return res

def find_season(seaname):
    """
    >>> find_season('DJ')
    (12, 1)
    >>> find_season('DJF')
    (12, 1, 2)
    >>> find_season('JA')
    (7, 8)
    >>> find_season('JJA')
    (6, 7, 8)
    >>> find_season('JJAS')
    (6, 7, 8, 9)
    >>> find_season('JJASO')
    (6, 7, 8, 9, 10)
    >>> find_season('JJASD')
    Traceback (most recent call last):
        ...
    KeyError: 'JJASD'
    >>> find_season('MAM')
    (3, 4, 5)
    """
    assert isinstance(seaname, str)
    seaname = seaname.upper()
    length = len(seaname)
    seats = possible_seasons(length=length)
    res = seats.loc[seaname]
    return res

# ~ def year_season(year, month, **kwwargs):
    # ~ """
    # ~ Function doc
    # ~ >>> 'afarie'
    # ~ """
    # ~ sea_ts = seasons_df(**kargs)
    # ~ res
    

##======================================================================================================================##
##                DAILY CYCLE                                                                                           ##
##======================================================================================================================##

def daily_cycle_df(n):
    """
    >>> daily_cycle_df(2)
    0     am
    1     am
    2     am
    3     am
    4     am
    5     am
    6     am
    7     am
    8     am
    9     am
    10    am
    11    am
    12    pm
    13    pm
    14    pm
    15    pm
    16    pm
    17    pm
    18    pm
    19    pm
    20    pm
    21    pm
    22    pm
    23    pm
    dtype: object
    >>> daily_cycle_df(4)
    0     ng
    1     ng
    2     ng
    3     ng
    4     ng
    5     ng
    6     am
    7     am
    8     am
    9     am
    10    am
    11    am
    12    pm
    13    pm
    14    pm
    15    pm
    16    pm
    17    pm
    18    ev
    19    ev
    20    ev
    21    ev
    22    ev
    23    ev
    dtype: object
    """
    if n == 2:
        vals = [_am, _pm]
    elif n == 4:
        vals = [_ng, _am, _pm, _ev]
    else:
        raise ValueError("Not yet implemented")

    #
    hours = numpy.arange(H_IN_1D)
    lngt = int(H_IN_1D / n)
    vals = [[i] * lngt for i in vals]
    vals = numpy.concatenate(vals)
    #
    res = pandas.Series(vals, index=hours)
    return res

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
