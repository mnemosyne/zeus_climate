"""
Read the config files
"""



##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import os
import glob
import argparse
import doctest

import pandas
import hjson



##======================================================================================================================##
##                ##
##======================================================================================================================##

_dir = os.path.dirname(__file__)
_box_pat = "box*.csv"
_per_pat = "per*.json"


##======================================================================================================================##
##                FUNCTIONS                                                                                             ##
##======================================================================================================================##

def std_boxname(boxname):
    """
    >>> std_boxname("East sahel")
    'East Sahel'
    """
    res = boxname.replace("_", " ")
    res = res.title()
    return res


def search_box_conf():
    """see load_boxes"""
    res = glob.glob(os.path.join(_dir, _box_pat))
    assert len(res) == 1, f"pb with box conf\n{res}"
    res = res[0]
    return res

def search_period_conf():
    """See load_periods"""
    res = glob.glob(os.path.join(_dir, _per_pat))
    assert len(res) == 1, f"pb with box conf\n{res}"
    res = res[0]
    return res

def load_boxes():
    """
    >>> load_boxes()
                       lon0   lon1  lat0  lat1
    Boxes                                     
    Africa            -25.0   55.0 -40.0  60.0
    Africa1           -20.0   55.0 -40.0  40.0
    Africa2           -20.0   50.0 -30.0  30.0
    Amma-Catch Benin    1.4    2.8   8.9  10.3
    Amma-Catch Niger    1.6    3.1  12.9  13.9
    Central Sahel     -10.0   -2.0  11.0  16.0
    Central Swa        -1.0    3.0   4.0  12.0
    Coastal           -10.0   10.0   4.0   7.0
    East Sahel         -2.0    5.0  11.0  16.0
    East Swa            3.0   10.0   4.0  12.0
    Ghana              -4.0    2.0   4.0  12.0
    Inland            -10.0   10.0   7.0   9.0
    Sahel             -18.0    5.0  11.0  16.0
    Sudan             -10.0   10.0   9.0  12.0
    Swa               -10.0   10.0   4.0  12.0
    Swalong           -10.0   10.0   4.0  12.0
    West Africa       -20.0   15.0   4.0  20.0
    West Sahel        -18.0  -10.0  11.0  16.0
    West Swa          -10.0   -1.0   4.0  12.0
    Wlat48            -10.0   -2.0   4.0   8.0
    Wlat812           -10.0   -2.0   8.0  12.0
    Wlon-10-6         -10.0   -6.0   4.0  12.0
    Wlon-6-2           -6.0   -2.0   4.0  12.0
    World            -180.0  180.0 -90.0  90.0

    """
    csvfile = search_box_conf()
    res = pandas.read_csv(csvfile, comment="#")
    res = res.set_index(res.columns[0])
    res = res.applymap(float)
    res.index = res.index.map(std_boxname)
    res = res.sort_index()
    return res

def load_periods():
    """
    >>> sorted(load_periods())
    ['period', 'periods', 'subperiods']
    """
    jsonfile = search_period_conf()
    with open(jsonfile, 'r', encoding="utf8") as jsrd:
        res = hjson.load(jsrd)
        
    return res

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTEST                    #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
