"""
For create and ploting climate indices at different timescale
"""

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import datetime
import doctest
from numbers import Number

import numpy
import pandas
from matplotlib import pyplot
import scipy.stats

from statsmodels.nonparametric.smoothers_lowess import lowess

from hades_stats import Sample, Trend

##======================================================================================================================##
##                               functions                                                                              ##
##======================================================================================================================##

def cooling_degree_day(x, threshold=25):
    """
    Compute cooling degree day from temperature serie in Celsius
    >>> cooling_degree_day([20, 21])
    0.0
    >>> cooling_degree_day([20, 27])
    2.0
    >>> cooling_degree_day([20, 27, 30])
    7.0
    >>> cooling_degree_day([20, 27, 30], threshold=20)
    17.0
    """
    x = numpy.array(x, dtype=float)
    cdd = x - threshold
    cdd = cdd[cdd>0]
    res = cdd.sum()
    return res
    
    
def heating_degree_day(x, threshold=19):
    """
    Compute heating degree day from temperature serie in Celsius
    >>> heating_degree_day([20, 21])
    0.0
    >>> heating_degree_day([20, 18])
    1.0
    >>> heating_degree_day([20, 18, 15])
    5.0
    >>> heating_degree_day([20, 18, 15], threshold=20)
    7.0
    """
    x = numpy.array(x, dtype=float)
    hdd = threshold - x
    hdd = hdd[hdd>0]
    res = hdd.sum()
    return res


def hydroclimatic_intensity(intensity, number):
    """
    >>> hydroclimatic_intensity(15,50)
    0.3
    >>> hydroclimatic_intensity(10,100)
    0.1
    >>> hydroclimatic_intensity(7,70)
    0.1
    """
    res = intensity / number
    return res

def split_process_in_spells(process, kind = slice):
    """
    Process : series of dtype=bool
    >>> "Todo"
    """
    assert isinstance(process, pandas.Series)
    assert process.dtype == bool
    assert (process.sort_index() == process).all()
    
    spells = []
    
    inspell = False
    idx0 = None
    idx1 = None
    spell = None
    idx = None
    
    for idx in process.index:
        d = process.loc[idx]
        if d == True:
            # print(d)
            if inspell:
                pass
            else:
                idx0 = idx
                inspell = True
        elif d == False:
            if inspell:
                spell = slice(idx0, idxold) 
                spells.append(spell)
                inspell = False
            else:
                pass
        else:
            raise ValueError(f"got {d=}")
        idxold = idx
    
    del inspell, idx0, idx1, idx, spell
    
    spells_series = pandas.concat([process.loc[s] for s in spells])
    
    assert (spells_series.index == process[process].index).all()
    assert process.sum() == spells_series.sum()
    
    res = {ispell: spell for ispell, spell in enumerate(spells)}

    if kind is slice:
        pass
    elif kind is pandas.Series or kind in ("ts", "Series", "series"):
        res = {ispell: process.loc[islc] for ispell, islc in res.items()}
    elif kind in ("concat", "all", "concatenated"):
        res = spells_series
    else:
        raise ValueError(f"Dont knwo what to do with {kind=}")
    
    return res

##======================================================================================================================##
##               ANNUAL STD INDEX CLASS                                                                                 ##
##======================================================================================================================##


class StdClimateIndex(Sample):
    """ 
    Standard Climate Index
    An value per year
    This index follow a Normal Distribution (0, 1)
    If the given values follow a normal distribution then values are standardized ([x - mu] /std)
    else an anamorphose can be a solution : not yet implemented
    >>> import time
    >>> x  = numpy.arange(100)
    >>> x = numpy.sin(x) + x / 100
    >>> n  = 50
    >>> x  = numpy.arange(n)
    >>> x  = numpy.sin(x) + (x / n*5)
    >>> dates = [datetime.datetime(iyear, 1,1) for iyear in numpy.arange(n) + 1900]
    >>> si = StdClimateIndex(x,  index = dates)
                
    >>> rawind = "centred"
    >>> fig = pyplot.figure()
    >>> l=si.plot(kind = rawind)
    >>> l=si.plot(kind = rawind, rolling_mean = 3, color = "r")
    >>> l=si.plot(kind = rawind, lowess = 5, ls = '--')
    >>> l=si.plot_mean(kind = rawind)
    >>> leg = pyplot.legend()

    >>> fig.show()
    >>> time.sleep(0.2)
    >>> pyplot.close()
    >>> dates = numpy.arange(n) + 1900
    >>> si = StdClimateIndex(x,  index = dates)
                
    >>> rawind = "std"
    >>> fig = pyplot.figure()
    >>> l=si.plot(kind = rawind)
    >>> l=si.plot(kind = rawind, lowess = 3, color = "r")
    >>> l=si.plot(kind = rawind, rolling_mean = 5, ls = '--')
    >>> l=si.plot_mean(kind = rawind)
    >>> leg = pyplot.legend()
    >>> a=trend = si.trend(kind = rawind)
    >>> sl = trend.plot()

    >>> fig.show()
    >>> time.sleep(0.2)
    >>> pyplot.close()
    """
    
    def __init__(self, data, **kwargs):
        """ Class initialiser """        
        Sample.__init__(self, data, **kwargs)
        years = self.years()
        assert years.size == len(set(years)), "Std_clim_Index must be an annual series"


    def std_index(self, model="norm"):
        """
        Return standardize index
        """
        if model == "norm":
            nortest = scipy.stats.mstats.normaltest(self)
            if nortest.pvalue < 0.05:
                print("Warning : can be Non normal:{0:.3f}".format(nortest.pvalue))
            vals = self.std_values()
        else:
            assert 0, "not implemented"
        
        res = pandas.Series(vals, index=self.index)
        
        return res


    def years(self):
        """
        Return the years depending if int are given or dates
        """
        v0 = self.index[0]
        if hasattr(v0, "year"):
            res = self.index.year
        else:
            res = numpy.array(self.index, dtype=int)
        
        return res
        
    def select_kind(self, kind):
        """
        Select kind among 'raw', 'centred' 'std'
        """
        if kind == "raw":
            res = self
        elif kind in ("centred", "anom"):
            res = self.centred_values()
        elif kind == "std":
            res = self.std_index()
        else:
            raise ValueError("Not implemented : {0}".format(kind))
            
        return res
        
    
    def plot(self, kind="raw", **kwargs):
        """Time series plot of the index"""
        #values to plot
        x = self.years()
        y = self.select_kind(kind=kind)
        
        if "rolling_mean" in kwargs:
            moving_wd = kwargs.pop("rolling_mean")
            assert isinstance(moving_wd, Number)
            assert moving_wd > 1 and moving_wd % 2 == 1, "exp > 1 and uneven number"
            moving_wd = int(moving_wd)
            y = y.rolling(window=moving_wd, center=True).mean()
            
        elif "lowess" in kwargs:
            moving_wd = kwargs.pop("lowess")
            assert isinstance(moving_wd, Number)
            assert moving_wd > 1
            frc = moving_wd / x.size
            y = lowess(y, x, frac=frc, return_sorted=False)
        
        else:
            moving_wd = False
        
        
        
        #ARGDIC
        if "bars" in kwargs:
            bars = bool(kwargs.pop("bars"))
        elif moving_wd:
            bars = False
        else:
            bars = True

        if bars:
            w = min(x[1:] - x[:-1])
            argdic = {"align" : "center", "width" :  w, "fc" : '0.5', 'ec' : "k"}
        else:
            argdic = {'color' : 'k', "ls" : "--", "lw" : 2}
            
        if moving_wd:
            argdic["label"] = "{0}yr".format(moving_wd)

        argdic.update(kwargs)

        #PLOT
        if bars:
            res = pyplot.bar(x, y, **argdic)
        else:
            res = pyplot.plot(x, y, **argdic)[0]
            
        #labels
        pyplot.xlabel("Years")
        pyplot.ylabel(kind)
            
        return res
        
    def plot_mean(self, kind="raw", **kwargs):
        """Plot the mean value of index or values"""
        x = self.years()
        y = self.select_kind(kind=kind)
            
        xmean = (x[0], x[-1])
        ymean = y.mean()
        ymean = [ymean] * 2
        
        argdic = {'color' : 'k', "lw" : 3, 'ls' : '-'}
        argdic.update(kwargs)

        res = pyplot.plot(xmean, ymean, **argdic)[0]
        return res

    def trend(self, kind="raw", typ="spearman"):
        """Return a trend estimation on index"""
        vals = self.select_kind(kind=kind)
        res = Trend(vals, typ=typ)
        return res


##======================================================================================================================##
##                DAILY SERIES                                                                                          ##
##======================================================================================================================##



class DailyData(object):
    """ Class doc
    >>> 'afarie' """
    
    def __init__(self):
        """ Class initialiser """
        pass


class DailyTemperature(DailyData):
    """ Class doc
    >>> 'afarie' """
    
    def __init__(self):
        """ Class initialiser """
        pass

class DailyRainfall(DailyData):
    """ Class doc
    >>> 'afarie' """
    
    def __init__(self):
        """ Class initialiser """
        pass

class DailyDischarge(DailyData):
    """ Class doc
    >>> 'afarie' """
    
    def __init__(self):
        """ Class initialiser """
        pass

class DailyWaterTable(DailyData):
    """ Class doc
    >>> 'afarie' """
    
    def __init__(self):
        """ Class initialiser """
        pass

##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        test_result = doctest.testmod()
        print(test_result)
        
        
    #+++++++++++++++++++++++++++++++#
    #    TESTS                      #
    #+++++++++++++++++++++++++++++++#
    
    if opts.examples:
        from japet_misc import show_all, close_all
        
        close_all()
        
        
        n = 100
        xi = numpy.arange(n)
        xi = numpy.sin(xi) + (xi / n*10)
        si = StdClimateIndex(xi, index=numpy.arange(n) + 1900)
        dates = [datetime.datetime(iyear, 1, 1) for iyear in numpy.arange(n) + 1900]
                
        for rawind in (True, False):
            pyplot.figure()
            si.plot(raw=rawind)
            si.plot(raw=rawind, lowess=3, color="r")
            si.plot(raw=rawind, lowess=5, color="b")
            si.plot(raw=rawind, rolling_mean=5, ls='--')
            si.plot_mean(raw=rawind)
            pyplot.legend()

        show_all()
