"""
Boxes Definition
 - Sahel
"""


##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
#~ import datetime
#~ import string

#~ import numpy
import pandas
from matplotlib import pyplot

from zeus_climate.read_conf import load_boxes, std_boxname

try:
    from obelibix.spatialobj import Rectangle
except:
    print("Need enhancement", __file__)
    print("import error Rectangle")

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

LONKEY = 'lon'
LATKEY = 'lat'


_boxes = load_boxes()
_lonkeys = [i for i in _boxes.columns if LONKEY in i]
_latkeys = [i for i in _boxes.columns if LATKEY in i]


##======================================================================================================================##
##                LOAD BOXES                                                                                            ##
##======================================================================================================================##

def is_box(boxname):
    """
    >>> is_box("sahel")
    True
    >>> is_box("WA")
    False
    """
    boxname = std_boxname(boxname)
    res = boxname in _boxes.index
    return res

##======================================================================================================================##
##                CLASSES BOX                                                                                           ##
##======================================================================================================================##

class Box(pandas.Series):
    """ Class doc
    >>> Box("sahel") 
    lon    (-18.0, 5.0)
    lat    (11.0, 16.0)
    Name: Sahel, dtype: object
    >>> Box("east_sahel")
    lon     (-2.0, 5.0)
    lat    (11.0, 16.0)
    Name: East Sahel, dtype: object
    """
    
    def __init__(self, boxname):
        """ Class initialiser """
        boxname = std_boxname(boxname)
        ser = _boxes.loc[boxname]
        lons = tuple(ser.loc[_lonkeys])
        lats = tuple(ser.loc[_latkeys])
        assert lons[0] < lons[1] and lats[0] < lats[1], f"pb ordered lon and lat: {lons} {lats}"
        dic = {LONKEY : lons, LATKEY: lats}
        pandas.Series.__init__(self, dic, index=(LONKEY, LATKEY), name=boxname)


    def to_rectangle(self):
        """
        >>> rec = Box("sahel").to_rectangle()
        """
        res = Rectangle(xlim=self.loc[LONKEY], ylim=self.loc[LATKEY])
        return res


##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        import doctest
        test_result = doctest.testmod()
        print(test_result)

    #+++++++++++++++++++++++++++++++#
    #    EXAMPLE                    #
    #+++++++++++++++++++++++++++++++#
    
    if opts.examples:
        fig = pyplot.figure()
        ax = fig.gca()
        af = Box("africa")
        for boxn in ("west_africa", "west sahel", "central sahel", "east sahel", "amma-catch niger", "amma-catch benin"):
            box = Box(boxn)
            rec = box.to_rectangle()
            rec.plot(fc='none')
        
        ax.set(xlim=af.loc[LONKEY], ylim=af.loc[LATKEY], aspect="equal")
        pyplot.grid(1)
        
        fig.show()
