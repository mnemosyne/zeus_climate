"""
Standard periods
 - West African 
"""

##======================================================================================================================##
##                PACKAGES                                                                                              ##
##======================================================================================================================##

import argparse
import datetime
import string
from collections.abc import Iterable

import numpy
import pandas
from matplotlib import pyplot

from zeus_climate.read_conf import load_periods

##======================================================================================================================##
##                CONSTANTS                                                                                             ##
##======================================================================================================================##

_psufix = "P"

_start = "start"
_end = "end"

_period_key = "period"
_periods_key = "periods"
_merge_sep = "**"
_subper_key = "subperiods"

_now_keys = ("now", "today")
_now = datetime.datetime.now().year

_indx_period = (_start, _end)
_col_periods = (_start, _end)

##======================================================================================================================##
##                CONF TO CONSTANTS                                                                                     ##
##======================================================================================================================##


_period_conf = load_periods()
_period_conf = {k1.lower() : {k2.lower() : val2 for k2, val2 in dic1.items()} for k1, dic1 in _period_conf.items()}

_dic_period = _period_conf[_period_key]
_dic_periods = _period_conf[_periods_key]
_dic_subperiods = _period_conf[_subper_key]


##======================================================================================================================##
##                PERIOD FUNCTIONS                                                                                      ##
##======================================================================================================================##

def std_period(per):
    """
    >>> std_period([1986,2015])
    (1986, 2015)
    >>> std_period([1986,"now"])
    (1986, 2023)
    >>> std_period(["now", 2015])
    Traceback (most recent call last):
      ...
    AssertionError: Uncoherent sorting (2023, 2015)

    """
    assert isinstance(per, Iterable)
    assert len(per) == 2
    
    res = [int(_now) if i in _now_keys else int(i) for i in per]
    res = tuple(res)
    
    assert res[1] > res[0], "Uncoherent sorting {0}".format(res)
    return res

def get_period(per):
    """
    >>> get_period("rg")
    (1950, 2014)
    >>> get_period("sat_IR")
    (1983, 2023)
    >>> get_period("sat_IR**rg")
    (1983, 2014)
    """
    if _merge_sep in per:
        pers = per.split(_merge_sep)
        #~ print(pers)
        periods = [get_period(per=iper) for iper in pers]
        res = merge_periods(*periods)
    else:
        per = per.lower()
        assert per in _dic_period, "{0} not in available keys\n{1}".format(per, sorted(_dic_period.keys()))
        vls = _dic_period[per]
        res = std_period(vls)
        
    return res
    
    
def merge_periods(*args):
    """
    >>> merge_periods((2015,2016))
    Traceback (most recent call last):
        ...
    AssertionError: Not period to concat ((2015, 2016),)
    >>> merge_periods((1950,2010), (1955,2011), (1983, "now"))
    (1983, 2010)
    """
    assert len(args) > 1, "Not period to concat {0}".format(args)
    periods = [std_period(per) for per in args]
    yrs0 = [per[0] for per in periods]
    yrs1 = [per[1] for per in periods]
    mn_yr = max(yrs0)
    mx_yr = min(yrs1)
    res = (mn_yr, mx_yr)
    return res
    
def period_slice(period, fmt=str):
    """
    >>> period_slice(None)
    slice(None, None, None)
    >>> period_slice(slice(2015, None))
    slice(2015, None, None)
    >>> period_slice((2015,2018))
    slice('2015', '2018', None)
    >>> period_slice((2015,2018), int)
    slice(2015, 2018, None)
    >>> period_slice((2015,2014))
    Traceback (most recent call last):
        ...
    AssertionError: pb bin expected, got (2015, 2014)
    """
    if period is None:
        res = slice(None)
    elif isinstance(period, slice):
        res = period
    else:
        assert len(period) == 2 and period[0] < period[1], f"pb bin expected, got {period}"
        period = [fmt(i) for i in period]
        res = slice(*period)
        
    return res

##======================================================================================================================##
##                PERIOD CLASS                                                                                          ##
##======================================================================================================================##

class Period(pandas.Series):
    """ Class doc
    >>> Period("rg")
    start    1950
    end      2014
    Name: rg, dtype: int64
    
    >>> Period("sat_IR")
    start    1983
    end      2023
    Name: sat_IR, dtype: int64
    
    >>> Period("rg**sat_IR")
    start    1983
    end      2014
    Name: rg**sat_IR, dtype: int64
    
    >>> per = Period("rg**sat_IR")
    >>> Period(per)
    start    1983
    end      2014
    Name: rg**sat_IR, dtype: int64

    
    >>> Period({"start": 1999, "end":2016}, name="nimp", index=('end', 'start'))
    Traceback (most recent call last):
        ...
    AssertionError: index is not what is expected
    ('end', 'start') vs ('start', 'end')
    >>> Period({"start": 2016, "end":2014}, name="nimp", index=('start', 'end'))
    Traceback (most recent call last):
        ...
    AssertionError: Uncoherent sorting [2016 2014]

    >>> Period({"start": 1999, "end":2016}, name="nimp", index=('start', 'end'))
    start    1999
    end      2016
    Name: nimp, dtype: int64
    >>> Period({"start": "1999", "end":"2016"}, name="nimp", index=('start', 'end'))
    start    1999
    end      2016
    Name: nimp, dtype: int64
    >>> Period({"start": 1999.5, "end":2016.5}, name="nimp", index=('start', 'end'))
    Traceback (most recent call last):
        ...
    AssertionError: pb float dtype. integer expected
    
    >>> z=Period('rg')
    >>> (z == Period(z)).all()
    True
    """
    
    def __init__(self, *args, **kwargs):
        """ Class initialiser """
        if len(args) == 1 and isinstance(args[0], str):
            #~ print(args)
            pername = args[0]
            #~ assert not args, "no args are expected when period is given got \n{0}".foramt(args)
            assert not kwargs, "no kwargs are expected when period is given got \n{0}".format(kwargs)
            peryrs = get_period(pername)
            
            obj = pandas.Series(peryrs, index=_indx_period, name=pername)
        else:
            obj = pandas.Series(*args, **kwargs)
            if obj.dtype == object:
                obj = obj.apply(float)
            
            if "float" in str(obj.dtype):
                assert (obj.apply(lambda i: i.is_integer())).all(), "pb float dtype. integer expected".format(obj.dtype)
            
            
        pandas.Series.__init__(self, obj, dtype=int)
        
        assert self.name is not None, "Please give a name got none"
        assert tuple(self.index) == _indx_period, "index is not what is expected\n{0} vs {1}".format(tuple(self.index), _indx_period)
        assert self[_end] > self[_start], "Uncoherent sorting {0}".format(self.values)


##======================================================================================================================##
##                SUB PERIODS   FUNCTIONS                                                                                         ##
##======================================================================================================================##



def std_breaks(breaks):
    """
    >>> std_breaks([2010,2013, 2016])
    array([2010, 2013, 2016])
    >>> std_breaks([2010,2013, "now"])
    array([2010, 2013, 2023])
    >>> std_breaks([2010,2009, "now"])
    Traceback (most recent call last):
        ...
    AssertionError: Uncoherent sorting [2010 2009 2023]
    """
    assert isinstance(breaks, Iterable)
    assert len(breaks) >= 3
    
    res = [int(_now) if i in _now_keys else int(i) for i in breaks]
    res = numpy.array(res, dtype=int)
    
    assert sorted(res) == list(res), "Uncoherent sorting {0}".format(res)
    return res

def auto_periods_index(nper):
    """
    >>> auto_periods_index(3)
    ['P1', 'P2', 'P3']
    >>> auto_periods_index(5)
    ['P1', 'P2', 'P3', 'P4', 'P5']
    """
    res = ["{0}{1}".format(_psufix, i+1) for i in range(nper)]
        
    return res
    
def get_periods(key):
    """
    >>> get_periods("wa")
    array([1950, 1970, 1990, 2023])
    """
    key = key.lower()
    assert key in _dic_periods, "{0} not in available keys\n{1}".format(key, sorted(_dic_periods.keys()))
    bks = _dic_periods[key]
    res = std_breaks(bks)
    
    return res
    
##======================================================================================================================##
##                PERIODS CLASS                                                                                         ##
##======================================================================================================================##



class Periods(pandas.DataFrame):
    """ Class doc
    >>> per_df = Periods('wa')
    >>> per_df
        start   end
    wa             
    P1   1950  1970
    P2   1970  1990
    P3   1990  2023

    >>> per_df.index.name
    'wa'
    >>> per_df.rm_overlap()
        start   end
    wa             
    P1   1950  1969
    P2   1970  1989
    P3   1990  2023

    >>> per_df.rm_overlap().change_date_fmt()
             start         end
    wa                        
    P1  01-01-1950  31-12-1969
    P2  01-01-1970  31-12-1989
    P3  01-01-1990  31-12-2023

    >>> per_df1 = per_df.add_subperiod(2000)
    >>> (per_df1 == per_df.add_subperiod()).all().all()
    True
    
    >>> per_df1
         start   end
    P1    1950  1970
    P2    1970  1990
    P3    1990  2023
    P3a   1990  2000
    P3b   2000  2023
    >>> per_df1.rm_overlap()
         start   end
    P1    1950  1969
    P2    1970  1989
    P3    1990  2023
    P3a   1990  1999
    P3b   2000  2023
    >>> per_df1.rm_overlap().change_date_fmt()
              start         end
    P1   01-01-1950  31-12-1969
    P2   01-01-1970  31-12-1989
    P3   01-01-1990  31-12-2023
    P3a  01-01-1990  31-12-1999
    P3b  01-01-2000  31-12-2023
    >>> per_df1.colors()
    P1        darkblue
    P2       goldenrod
    P3             red
    P3a      orangered
    P3b    darkmagenta
    dtype: object
    >>> per_df1.colors(P1='dark')
    P1            dark
    P2       goldenrod
    P3             red
    P3a      orangered
    P3b    darkmagenta
    dtype: object
    
    
    >>> decades = Periods(numpy.arange(1955,2023,10))
    >>> decades
        start   end
    P1   1955  1965
    P2   1965  1975
    P3   1975  1985
    P4   1985  1995
    P5   1995  2005
    P6   2005  2015
    
    >>> decades.pop_period('P2', reindex=False)
        start   end
    P1   1955  1965
    P3   1975  1985
    P4   1985  1995
    P5   1995  2005
    P6   2005  2015

    
    >>> decades.pop_period('P2', reindex=True)
        start   end
    P1   1955  1965
    P2   1975  1985
    P3   1985  1995
    P4   1995  2005
    P5   2005  2015
    """
    
    def __init__(self, obj, **kwargs):
        """ Class initialiser """
        #~ print(obj)
        if isinstance(obj, (pandas.DataFrame, Periods)):
            obj = pandas.DataFrame(obj, **kwargs)
        else:
            assert not kwargs, 'Expect no kwargs, got {0}'.format(kwargs)
            if isinstance(obj, str):
                name = obj
                #~ print(obj)
                breaks = get_periods(obj)
            elif isinstance(obj, Iterable):
                breaks = std_breaks(breaks=obj)
                name = None
            else:
                raise ValueError("Dont know what to do with\n{0}\n{1}".format(obj, kwargs))
            
            starts = breaks[:-1]
            ends = breaks[1:]
            nindx = len(breaks) - 1
            data = {_start : starts, _end : ends}
            indx = auto_periods_index(nindx)
            
            obj = pandas.DataFrame(data, columns=_col_periods, index=indx)
            obj.index.name = name
        
        pandas.DataFrame.__init__(self, obj)


        assert sorted(self.index) == list(self.index), "pb index\n{0}".format(self.index)
        assert all([i[0] == _psufix for i in self.index]), "pb index\n{0}".format(self.index)
        assert tuple(self.columns) == _col_periods, "pb columns\n{0}\n{1}".format(self.columns, _col_periods)
                
    def rm_overlap(self):
        """Remove overlat years"""
        starts = self[_start].values
        ends = self[_end].values
        #~ print(starts, ends)
        #~ starts = ["01-01-{0}".format(iyr) for iyr in starts]
        #~ print([iyr for iyr in ends if iyr in starts])
        ends = [iyr - 1 if iyr in starts else iyr for iyr in ends]
        data = {_start : starts, _end : ends}
        new = pandas.DataFrame(data, columns=self.columns, index=self.index)
        res = Periods(new)
        return res
        

    def add_subperiod(self, cut_year=None):
        """Add a subperiod"""
        if cut_year is None:
            autokey = str(self.index.name).lower()
            cut_year = _dic_subperiods[autokey]
        
        cut_year = int(cut_year)
        
        which = self.apply(lambda x: x[0] < cut_year < x[1], axis=1)
        assert which.sum() == 1
        period = str(which[which].index[0])
        #~ print(period)
        start = self.loc[period][_start]
        end = self.loc[period][_end]
        #~ print(start, end)
        newstarts = [start, cut_year]
        newends = [cut_year, end]
        #
        starts = self[_start]
        ends = self[_end]
        #
        starts = numpy.concatenate([starts, newstarts])
        ends = numpy.concatenate([ends, newends])
        #~ print(starts)
        #~ print(ends)
        
        newidx = ["{0}{1}".format(period, string.ascii_lowercase[ilet]) for ilet in range(2)]
        newidx = numpy.concatenate([self.index, newidx])
        
        data = {_start : starts, _end : ends}
        new = pandas.DataFrame(data, columns=self.columns, index=newidx)
        res = Periods(new)
        return res

    def change_date_fmt(self):
        """chnge the date format"""
        starts = self[_start]
        ends = self[_end]
        starts = ["01-01-{0}".format(iyr) for iyr in starts]
        ends = ["31-12-{0}".format(iyr) for iyr in ends]
        data = {_start : starts, _end : ends}
        new = pandas.DataFrame(data, columns=self.columns, index=self.index)
        res = Periods(new)
        return res
        
    def colors(self, **kwargs):
        """Attribute colors to periods"""
        colors = ["darkblue", "goldenrod", "red", "orangered", "darkmagenta"]
        data = {per : col for per, col in zip(self.index, colors)}
        data.update(kwargs)
        
        res = pandas.Series(data, index=self.index)
        return res
        
    def pop_period(self, key, reindex=True):
        """Remove a periodd"""
        data = self.loc[[i for i in self.index if i != key]]
        if reindex:
            newidx = ["{0}{1}".format(_psufix, i+1) for i in range(data.index.size)]
        else:
            newidx = data.index
        new = pandas.DataFrame(data.values, columns=self.columns, index=newidx)
        res = Periods(new)
        return res
        
##======================================================================================================================##
##                MAIN                                                                                                  ##
##======================================================================================================================##

if __name__ == '__main__':
    
    #+++++++++++++++++++++++++++++++#
    #    OPTIONS                    #
    #+++++++++++++++++++++++++++++++#
    
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--doctests', default=False, action='store_true')
    parser.add_argument('--examples', default=False, action='store_true')
    opts = parser.parse_args()
    
    #+++++++++++++++++++++++++++++++#
    #    DOCTESTS                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.doctests:
        import doctest
        test_result = doctest.testmod()
        print(test_result)


    #+++++++++++++++++++++++++++++++#
    #    EXAMPLES                   #
    #+++++++++++++++++++++++++++++++#
    
    if opts.examples:
        
        perds = Periods('wa')
        print(perds)
        #~ print(wa_periods(True, str))
        #~ print(wa_periods(False, str))
        perds = perds.add_subperiod()
        wa_period_colors = perds.colors()
        
        pyplot.close()
        fig = pyplot.figure()
        i = 0
        for p, c in sorted(wa_period_colors.items()):
            pyplot.plot([i, i + 1, i + 2], [0, 1, 0.5], ls="-", marker='o', color=c, lw=2)
            i += 1
            
        fig.show()
            

        period_df = Periods("wa")

        print(period_df)

        print(period_df.change_date_fmt())
        print(period_df.rm_overlap())
        print(period_df.add_subperiod().rm_overlap())
